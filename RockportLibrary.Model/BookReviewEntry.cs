﻿using System;

namespace RockportLibrary.Model
{
    public class BookReview
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public Guid BookId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
