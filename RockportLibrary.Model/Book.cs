﻿using System;

namespace RockportLibrary.Model
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ArtworkUrl {get;set;}
        public Guid Id { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; } = DateTime.Now;
    }
}
