﻿using System;

namespace RockportLibrary.Model
{
    public class BookEvent
    {
        public DateTime Date { get; set; }
        public Guid BookId { get; set; }

        /// <summary>
        /// When true, then book was returned, otherwise borrowed
        /// </summary>
        public bool IsIn { get; set; }
        public Guid UserId { get; set; }

    }
}
