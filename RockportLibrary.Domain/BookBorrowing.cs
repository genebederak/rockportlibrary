﻿using RockportLibrary.Domain.Contracts;
using RockportLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RockportLibrary.Domain
{
    public class BookBorrowing : IBookBorrowing
    {
        private readonly IQueryable<Book> _books;
        private readonly ICollection<BookEvent> _events;

        public BookBorrowing(IQueryable<Book> books, ICollection<BookEvent> events)
        {
            _books = books;
            _events = events;
        }
        public bool Borrow(Guid bookId, Guid userId)
        {
            var book = _books.SingleOrDefault(b=>b.Id == bookId);
            if (book == null)
                return false;

            var mostRecentEvent = _events
                .Where(b => b.BookId == book.Id)
                .OrderByDescending(x => x.Date)
                .FirstOrDefault();

            if (mostRecentEvent != null && !mostRecentEvent.IsIn)
                return false;

            _events.Add(new BookEvent {
                BookId = book.Id,
                Date = DateTime.Now,
                IsIn = false,
                UserId = userId
            });

            return true;
        }

        public bool ReturnBook(Guid bookId, Guid userId)
        {
            var book = _books.SingleOrDefault(b => b.Id == bookId);
            if (book == null)
                return false;

            var mostRecentEvent = _events
                .Where(b => b.BookId == book.Id)
                .OrderByDescending(x => x.Date)
                .FirstOrDefault();

            if (mostRecentEvent != null && mostRecentEvent.IsIn)
                return false;

            _events.Add(new BookEvent {
                BookId = book.Id,
                Date = DateTime.Now,
                IsIn = true,
                UserId = userId
            });

            return true;
        }
    }

}
