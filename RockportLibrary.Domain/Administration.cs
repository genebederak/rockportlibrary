﻿using RockportLibrary.Domain.Contracts;
using RockportLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RockportLibrary.Domain
{
    public class Administration : IBookAdministration
    {
        private ICollection<Book> _books;

        public Administration(ICollection<Book> books)
        {
            _books = books;
        }

        public Guid Add(NewBookEntry book)
        {
            var newId = Guid.NewGuid();

            _books.Add(new Book()
            {
                Id = newId,
                Title = book.Title,
                SubTitle = book.SubTitle,
                Author = book.Author,
                Description = book.Description,
                ArtworkUrl = book.ArtworkUrl,
                DateAdded = DateTime.Now
            });

            return newId;
        }

        public bool DeleteBook(Guid bookId)
        {
            var book = _books.SingleOrDefault(b => b.Id == bookId);

            if (book == null)
                return false;

            _books.Remove(book);
            return true;
        }
    }
}
