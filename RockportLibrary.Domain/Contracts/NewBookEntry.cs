﻿using System;

namespace RockportLibrary.Domain.Contracts
{
    public class NewBookEntry
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Author { get; set; }
        public string ArtworkUrl { get; set; }
        public string Description { get; set; }

    }
}
