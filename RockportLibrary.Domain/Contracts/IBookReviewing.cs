﻿using RockportLibrary.Model;
using System;

namespace RockportLibrary.Domain.Contracts
{
    public interface IBookReviewing
    {
        bool ReviewBook(BookReview review);  
    }

    
}
