﻿using System;

namespace RockportLibrary.Domain.Contracts
{
    public interface IBookBorrowing
    {
        bool Borrow(Guid bookId, Guid userId);
        bool ReturnBook(Guid bookId, Guid userId);

    }

}
