﻿using System;

namespace RockportLibrary.Domain.Contracts
{
    public interface IBookAdministration
    {
        Guid Add(NewBookEntry book);
        bool DeleteBook(Guid bookId);
    }

}
