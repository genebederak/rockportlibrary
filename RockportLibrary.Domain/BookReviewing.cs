﻿using RockportLibrary.Domain.Contracts;
using RockportLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockportLibrary.Domain
{
    public class BookReviewing : IBookReviewing
    {
        private readonly IQueryable<Book> _books;
        private readonly ICollection<BookReview> _reviews;

        public BookReviewing(IQueryable<Book> books, ICollection<BookReview> reviews)
        {
            _books = books;
            _reviews = reviews;
        }
        public bool ReviewBook(BookReview review)
        {
            var book = _books.SingleOrDefault(b => b.Id == review.BookId);
            if (book == null)
                return false;

            if (_reviews.Any(r => r.Id == review.Id))
                return false;

            _reviews.Add(review);

            return true;
        }
    }
}
