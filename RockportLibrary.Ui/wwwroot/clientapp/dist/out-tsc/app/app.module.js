import { __decorate } from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common'; //was getting very strange errors when this not imported (especially with nested components) when this not imported
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { RouterModule } from '@angular/router';
import { BookDetailsComponent } from './book-details/book-details.component';
import { AddBookComponent } from './add-book/add-book.component';
import { BookSuggestionComponent } from './book-suggestion/book-suggestion.component';
import { FormsModule } from '@angular/forms';
let AppModule = class AppModule {
};
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent,
            BookListComponent,
            BookDetailsComponent,
            AddBookComponent,
            BookSuggestionComponent
        ],
        imports: [
            RouterModule,
            AppRoutingModule,
            BrowserModule,
            CommonModule,
            HttpClientModule,
            FormsModule
        ],
        providers: [],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map