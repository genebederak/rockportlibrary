import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { GoogleBooksService } from './google-books-service';
let BookSuggestionComponent = class BookSuggestionComponent {
    constructor(service) {
        this.service = service;
        this.notifyBookSelected = new EventEmitter();
        this.suggestions = [];
    }
    ngOnChanges(changes) {
        this.refreshSuggestions();
    }
    refreshSuggestions() {
        if (!this.searchString || this.searchString.trim() == '') {
            this.suggestions = [];
            return;
        }
        this.search(this.searchString);
    }
    search(searchQuery) {
        this.service.search(searchQuery)
            .subscribe(s => { this.suggestions = s; }, error => { console.log(error); });
    }
    onBookSelected(book) {
        this.debug(book);
        this.notifyBookSelected.emit(book);
    }
    debug(book) {
        console.debug(`new Book
{
    Id = new Guid(""),
        Title = "${book.title}",
        SubTitle = "${book.subtitle ? book.subtitle : ""}",
        Author = "${book.author}",
        ArtworkUrl = "${book.coverUrl}",
        Description = "${book.description}"
}`);
    }
};
__decorate([
    Input()
], BookSuggestionComponent.prototype, "searchString", void 0);
__decorate([
    Output()
], BookSuggestionComponent.prototype, "notifyBookSelected", void 0);
BookSuggestionComponent = __decorate([
    Component({
        selector: "books-suggestions",
        templateUrl: "./book-suggestion.component.html",
        styleUrls: ['./book-suggestion.component.css'],
        providers: [GoogleBooksService]
    })
], BookSuggestionComponent);
export { BookSuggestionComponent };
//# sourceMappingURL=book-suggestion.component.js.map