import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
let GoogleBooksService = class GoogleBooksService {
    constructor(http) {
        this.http = http;
    }
    search(query) {
        return this.http.get("https://www.googleapis.com/books/v1/volumes?q=" + query)
            .pipe(map((result) => (result.items ? result.items.map(this.ToBookSuggestion) : [])));
    }
    ToBookSuggestion(obj) {
        return {
            title: obj.volumeInfo.title,
            subtitle: obj.volumeInfo.subtitle,
            author: obj.volumeInfo.authors ? obj.volumeInfo.authors[0] : "",
            description: obj.volumeInfo.description,
            coverUrl: obj.volumeInfo.imageLinks ? obj.volumeInfo.imageLinks.smallThumbnail : "",
        };
    }
};
GoogleBooksService = __decorate([
    Injectable({ providedIn: "root" })
], GoogleBooksService);
export { GoogleBooksService };
//# sourceMappingURL=google-books-service.js.map