import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AppComponent = class AppComponent {
    constructor() {
        this.title = 'The Rockport Library';
    }
};
AppComponent = __decorate([
    Component({
        selector: 'books-app',
        templateUrl: './app.component.html',
        styles: []
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map