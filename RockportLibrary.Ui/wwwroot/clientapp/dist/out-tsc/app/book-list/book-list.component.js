import { __decorate } from "tslib";
import { Component } from "@angular/core";
let BookListComponent = class BookListComponent {
    constructor(data) {
        this.data = data;
        this.selectedBook = null;
        this.books = [];
        this.books = data.books;
    }
    ngOnInit() {
        this.data.loadBooks().subscribe(success => {
            if (success) {
                this.books = this.data.books;
            }
        });
    }
    click(book) {
        console.log(`clicked ${book.bookId}`);
        this.selectedBook = book;
    }
};
BookListComponent = __decorate([
    Component({
        selector: "books-list",
        templateUrl: "./book-list.component.html",
        styleUrls: ["./book-list.component.css"],
    })
], BookListComponent);
export { BookListComponent };
//# sourceMappingURL=book-list.component.js.map