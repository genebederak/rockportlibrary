import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
let AddBookComponent = class AddBookComponent {
    constructor(data, router) {
        this.data = data;
        this.router = router;
        this.model = {
            title: "",
            subTitle: "",
            author: "",
            description: "",
            coverUrl: ""
        };
    }
    ngAfterViewInit() {
        [this.title.nativeElement, this.author.nativeElement]
            .map(element => fromEvent(element, 'keyup')
            .pipe(debounceTime(500), distinctUntilChanged(), tap(_ => this.updateSearchString()))
            .subscribe());
    }
    updateSearchString() {
        this.stringToSearch = this.model.title + " " + this.model.author;
    }
    onSubmit() {
        this.data.addBook({
            title: this.model.title,
            subTitle: this.model.subTitle,
            author: this.model.author,
            description: this.model.description,
            artworkUrl: this.model.coverUrl
        }).subscribe(createdBook => { this.router.navigate(["/books", createdBook.bookId]); }, error => { console.log(error); alert("sorry, could not create book"); });
    }
    clear() {
        Object.keys(this.model).forEach(key => this.model[key] = "");
    }
    onNotifyBookSelected(book) {
        this.model.title = book.title;
        this.model.author = book.author;
        this.model.subTitle = book.subtitle;
        this.model.description = book.description;
        this.model.coverUrl = book.coverUrl;
    }
};
__decorate([
    ViewChild('title')
], AddBookComponent.prototype, "title", void 0);
__decorate([
    ViewChild('author')
], AddBookComponent.prototype, "author", void 0);
AddBookComponent = __decorate([
    Component({
        templateUrl: "./add-book.component.html",
        styleUrls: ["./add-book.component.css"]
    })
], AddBookComponent);
export { AddBookComponent };
//# sourceMappingURL=add-book.component.js.map