import { __decorate } from "tslib";
import { Component, } from "@angular/core";
let BookDetailsComponent = class BookDetailsComponent {
    constructor(route, router, data) {
        this.router = router;
        this.data = data;
        this.bookHistory = [];
        this.bookId = route.snapshot.paramMap.get("id");
    }
    ngOnInit() {
        this.loadAll();
    }
    loadAll() {
        this.loadBook();
        this.loadHistory();
    }
    loadBook() {
        this.data.loadBook(this.bookId).subscribe(success => {
            if (success) {
                this.book = this.data.book;
            }
        });
    }
    loadHistory() {
        this.data.loadBookHistory(this.bookId).subscribe(success => {
            if (success) {
                this.bookHistory = this.data.bookHistory;
            }
        });
    }
    borrow() {
        this.data.borrowBook(this.bookId)
            .subscribe(success => this.loadAll(), error => { console.log(error); alert("an error occurred; sorry"); });
    }
    return() {
        this.data.returnBook(this.bookId)
            .subscribe(success => this.loadAll(), error => { console.log(error); alert("an error occurred; sorry"); });
    }
    delete() {
        this.data.deleteBook(this.bookId)
            .subscribe(success => this.router.navigate(['/']), error => { console.log(error); alert("an error occurred; sorry"); this.router.navigate(['/']); });
    }
};
BookDetailsComponent = __decorate([
    Component({
        templateUrl: "./book-details.component.html",
        styleUrls: ["./book-details.component.css"]
    })
], BookDetailsComponent);
export { BookDetailsComponent };
//# sourceMappingURL=book-details.component.js.map