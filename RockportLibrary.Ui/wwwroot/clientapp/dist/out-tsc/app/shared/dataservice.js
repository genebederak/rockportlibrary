import { __decorate } from "tslib";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
let DataService = class DataService {
    constructor(http) {
        this.http = http;
        this.bookHistory = [];
        this.books = [];
    }
    loadBooks() {
        return this.http.get("/api/books")
            .pipe(map((data) => { this.books = data; return true; }));
    }
    loadBookHistory(bookId) {
        return this.http.get("/api/books/" + bookId + "/history")
            .pipe(map((data) => { this.bookHistory = data; return true; }));
    }
    loadBook(id) {
        return this.http.get(`/api/books/${id}`)
            .pipe(map((data) => { this.book = data; return true; }));
    }
    borrowBook(id) {
        return this.http.patch(`/api/books/${id}/borrow`, {});
    }
    returnBook(id) {
        return this.http.patch(`/api/books/${id}/return`, {});
    }
    deleteBook(id) {
        return this.http.delete(`/api/books/${id}`, {});
    }
    addBook(book) {
        return this.http.post("/api/books", book)
            .pipe(map((data) => { this.books.push(data); return data; }));
    }
};
DataService = __decorate([
    Injectable({
        providedIn: "root"
    })
], DataService);
export { DataService };
//# sourceMappingURL=dataservice.js.map