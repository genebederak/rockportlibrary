﻿import { Component, OnInit, } from "@angular/core";
import { DataService } from '../shared/dataservice';
import { ActivatedRoute, Router } from '@angular/router';
import { BookEntry } from '../shared/book-entry.model';
import { BookHistoryEntry } from '../shared/book-history.model';

@Component({
    templateUrl: "./book-details.component.html",
    styleUrls: ["./book-details.component.css"]
})

export class BookDetailsComponent implements OnInit {
    constructor(route: ActivatedRoute, private router : Router, private data: DataService) {
        this.bookId = route.snapshot.paramMap.get("id");
    }

    public bookId: string;
    public book: BookEntry;
    public bookHistory: BookHistoryEntry[] = [];

    ngOnInit(): void {
        this.loadAll();
    }

    loadAll() {
        this.loadBook();
        this.loadHistory();
    }
    loadBook(): void {
        this.data.loadBook(this.bookId).subscribe(success => {
            if (success) {
                this.book = this.data.book;
            }
        })
    }
    loadHistory(): void {
        this.data.loadBookHistory(this.bookId).subscribe(success => {
            if (success) {
                this.bookHistory = this.data.bookHistory;
            }
        });
    }

    borrow(): void {
        this.data.borrowBook(this.bookId)
            .subscribe(
                success => this.loadAll(),
                error => { console.log(error); alert("an error occurred; sorry") }
            );

    }
    return(): void {
        this.data.returnBook(this.bookId)
            .subscribe(
                success => this.loadAll(),
                error => { console.log(error); alert("an error occurred; sorry") }
            );

    }

    delete(): void {
        this.data.deleteBook(this.bookId)
            .subscribe(
                success => this.router.navigate(['/']),
                error => { console.log(error); alert("an error occurred; sorry"); this.router.navigate(['/']) }
            );
     
    }

    
}


