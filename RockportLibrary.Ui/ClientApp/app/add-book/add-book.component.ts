﻿import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

import { BookSuggestion } from '../book-suggestion/book-suggestion.component';
import { DataService } from '../shared/dataservice';

@Component({
    templateUrl: "./add-book.component.html",
    styleUrls: ["./add-book.component.css"]
})
export class AddBookComponent implements AfterViewInit {

    @ViewChild('title') title: ElementRef;
    @ViewChild('author') author: ElementRef;
    

    constructor(private data: DataService, private router : Router) {
        
    }
    public model = {
        title: "",
        subTitle:"",
        author: "",
        description: "",
        coverUrl: ""
    }
    
    stringToSearch: string;
    
    ngAfterViewInit(): void {

        [this.title.nativeElement, this.author.nativeElement]
            .map(element => fromEvent(element, 'keyup')
                .pipe(
                    debounceTime(500),
                    distinctUntilChanged(),
                    tap(_ => this.updateSearchString())
                )
                .subscribe())
      
    }

    updateSearchString() {
        this.stringToSearch = this.model.title + " " + this.model.author;
    }

    onSubmit() {

        this.data.addBook({
            title: this.model.title,
            subTitle: this.model.subTitle,
            author: this.model.author,
            description: this.model.description,
            artworkUrl: this.model.coverUrl
        }).subscribe(
            createdBook => { this.router.navigate(["/books", createdBook.bookId]) },
            error => { console.log(error); alert("sorry, could not create book")}
        );
    }

    clear() {
        Object.keys(this.model).forEach(key => this.model[key]="");

    }
    onNotifyBookSelected(book: BookSuggestion) {
        this.model.title = book.title;
        this.model.author = book.author;
        this.model.subTitle = book.subtitle;
        this.model.description = book.description;
        this.model.coverUrl = book.coverUrl;

    }
    
}