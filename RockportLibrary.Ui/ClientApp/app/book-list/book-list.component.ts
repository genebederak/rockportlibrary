﻿import { Component, OnInit} from "@angular/core"; 
import { DataService } from '../shared/dataservice';
import { BookEntry } from '../shared/book-entry.model';

@Component({
    selector: "books-list",
    templateUrl: "./book-list.component.html",
    styleUrls: ["./book-list.component.css"],
    
    
})

export class BookListComponent implements OnInit {

    constructor(private data: DataService) {
        this.books = data.books;
    }

    public selectedBook: BookEntry = null;
    public books: BookEntry[] = [];


    ngOnInit(): void {
        this.data.loadBooks().subscribe(success => {
            if (success) {
                this.books = this.data.books;
            }
        }); 
    }

    click(book: BookEntry): void {
        console.log(`clicked ${book.bookId}`);
        this.selectedBook = book;
    }


}
