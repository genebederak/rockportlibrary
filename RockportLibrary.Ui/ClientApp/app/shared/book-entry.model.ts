﻿export interface BookEntry {
    bookId: string
    bookTitle: string;
    bookSubTitle: string;
    bookDescription: string;
    bookAuthor: string;
    bookArtworkUrl: string;
    isAvailable: boolean;
    doIHaveIt: boolean;
} 
export interface NewBookEntry {
    title: string;
    subTitle: string;
    description: string;
    author: string;
    artworkUrl: string;
}