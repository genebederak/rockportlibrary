﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { BookEntry, NewBookEntry } from './book-entry.model';
import { BookHistoryEntry } from './book-history.model';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: "root"
})
export class DataService {
    constructor(private http: HttpClient) {
        this._baseUrl = environment.apiUrl
    }

    loadBooks() {
        return this.http.get(`${this._baseUrl}/books`)
            .pipe(
                map((data: BookEntry[]) => { this.books = data; return true; })
            );
    }

    loadBookHistory(bookId: string) {
        return this.http.get(`${this._baseUrl}/books/` + bookId + "/history")
            .pipe(
                map((data: BookHistoryEntry[]) => { this.bookHistory = data; return true; })
            );
    }

    loadBook(id: string) {
        return this.http.get(`${this._baseUrl}/books/${id}`)
            .pipe(
                map((data: BookEntry) => { this.book = data; return true; })
            );
    }

    borrowBook(id: string) {
        return this.http.patch(`${this._baseUrl}/books/${id}/borrow`, {});
    }

    returnBook(id: string) {
        return this.http.patch(`${this._baseUrl}/books/${id}/return`, {});
    }

    deleteBook(id: string) {
        return this.http.delete(`${this._baseUrl}/books/${id}`, {});
    }

    addBook(book: NewBookEntry) {
        return this.http.post(`${this._baseUrl}/books`, book)
            .pipe(map((data: BookEntry) => { this.books.push(data); return data; }));
    }

    public bookHistory: BookHistoryEntry[] = [];
    public books: BookEntry[] = [];
    public book: BookEntry;
    private _baseUrl = "";
}