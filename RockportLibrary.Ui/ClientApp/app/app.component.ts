import { Component } from '@angular/core';

@Component({
  selector: 'books-app',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'The Rockport Library';
}
