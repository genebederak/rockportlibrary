import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookListComponent } from './book-list/book-list.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { AddBookComponent } from './add-book/add-book.component';

const routes: Routes = [
    { path: "", component: BookListComponent },
    { path: "books/:id", component: BookDetailsComponent },
    { path: "addbook", component: AddBookComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
            useHash: true,
            //enableTracing: true 
        })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 