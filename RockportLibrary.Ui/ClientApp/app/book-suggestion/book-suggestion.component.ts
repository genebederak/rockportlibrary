﻿import { Component, OnChanges, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { GoogleBooksService } from './google-books-service';

@Component({
    selector: "books-suggestions",
    templateUrl: "./book-suggestion.component.html",
    styleUrls: ['./book-suggestion.component.css'],
    providers: [GoogleBooksService]
})
export class BookSuggestionComponent implements OnChanges {
    
    constructor(private service: GoogleBooksService) {
        
    }

    @Input() searchString: string;
    @Output() notifyBookSelected: EventEmitter<BookSuggestion> = new EventEmitter<BookSuggestion>();
    suggestions: BookSuggestion[] = [];

    ngOnChanges(changes: SimpleChanges): void {
        this.refreshSuggestions();
    }

    refreshSuggestions() {
        if (!this.searchString || this.searchString.trim()=='') {
            this.suggestions = [];
            return;
        }
            

        this.search(this.searchString);
    }
    

    search(searchQuery: string) {
        this.service.search(searchQuery)
            .subscribe(
                s => { this.suggestions = s },
                error => { console.log(error); },
            )
    }

    onBookSelected(book: BookSuggestion) {
        this.debug(book)
        this.notifyBookSelected.emit(book);
    }

    debug(book: BookSuggestion) {
        console.debug(
`new Book
{
    Id = new Guid(""),
        Title = "${book.title}",
        SubTitle = "${book.subtitle ? book.subtitle : ""}",
        Author = "${book.author}",
        ArtworkUrl = "${book.coverUrl}",
        Description = "${book.description}"
}`);

    }
}

export interface BookSuggestion {
    title: string;
    subtitle: string;
    description: string;
    author: string;
    coverUrl: string;
}