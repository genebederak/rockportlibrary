﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BookSuggestion } from './book-suggestion.component';

@Injectable({ providedIn: "root" })
export class GoogleBooksService {
    constructor(private http: HttpClient) {

    }
    search(query: string): Observable<BookSuggestion[]>  {
        return this.http.get("https://www.googleapis.com/books/v1/volumes?q=" + query)
            .pipe(
                map((result: any) => (result.items ? result.items.map(this.ToBookSuggestion) : [] ))
            )
    } 

    private ToBookSuggestion(obj: any): BookSuggestion {
       
        return {
            title: obj.volumeInfo.title,
            subtitle: obj.volumeInfo.subtitle,
            author: obj.volumeInfo.authors ? obj.volumeInfo.authors[0] : "",
            description: obj.volumeInfo.description,
            coverUrl: obj.volumeInfo.imageLinks ? obj.volumeInfo.imageLinks.smallThumbnail : "",
        }
    }

}