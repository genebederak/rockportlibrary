﻿using RockportLibrary.Model;
using System;

namespace RockportLibrary.Read
{
    /// <summary>
    /// A Book Entry in the Library
    /// </summary>
    public class BookEntryModel
    {
        /// <summary>
        /// Unique identifier of the book
        /// </summary>
        public Guid BookId { get; private set; }
        /// <summary>
        /// The Title of the book
        /// </summary>
        public string BookTitle { get; private set; }
        public string BookSubTitle { get; private set; }
        public string BookAuthor { get; private set; }
        public string BookArtworkUrl { get; private set; }

        public string BookDescription { get; private set; }
        public bool IsAvailable { get; private set; }
        public bool DoIHaveIt { get; private set; }

        public static BookEntryModel Create(Book book, BookEvent lastHistory, Guid myUserId, bool includeDescription = false)
            => new BookEntryModel
            {
                BookId = book.Id,
                BookAuthor = book.Author,
                BookSubTitle = book.SubTitle,
                BookTitle = book.Title,
                BookDescription = includeDescription ? book.Description : null,
                BookArtworkUrl = book.ArtworkUrl,
                IsAvailable = lastHistory.IsIn,
                DoIHaveIt = !lastHistory.IsIn && lastHistory.UserId == myUserId
            };
    }
}
