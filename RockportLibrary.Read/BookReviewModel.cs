﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockportLibrary.Read
{
    public class BookReviewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Username { get; set; }
    }
}
