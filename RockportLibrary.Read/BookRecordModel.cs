﻿using System;

namespace RockportLibrary.Read
{
    public class BorrowRecordModel
    {
        public DateTime? TimeOut { get; set; }

        public DateTime? TimeIn { get; set; }
    }
}
