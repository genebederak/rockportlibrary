﻿using System;
using System.Collections.Generic;

namespace RockportLibrary.Read.contracts
{
    public interface IBookViewRepository
    {
        IEnumerable<BookEntryModel> GetAllBooks(Guid borrowerId);
        BookEntryModel FindById(Guid id, Guid borrowerId);
        IEnumerable<BorrowRecordModel> GetBorrowRecords(Guid bookId);

        IEnumerable<BookReviewModel> GetBookReviews(Guid bookId);

    }
}
