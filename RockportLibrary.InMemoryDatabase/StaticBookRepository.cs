﻿using RockportLibrary.Model;
using RockportLibrary.Read;
using RockportLibrary.Read.contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RockportLibrary.InMemoryDatabase
{

    public class StaticBookRepository : IBookViewRepository
    {
        
        
        private readonly IQueryable<Book> _books;
        private readonly IQueryable<BookEvent> _events;
        private readonly IQueryable<BookReview> _bookReviews;
        internal static Guid _systemAccount = new Guid("00000000-0000-0000-0000-000000000000");

        public StaticBookRepository
            (
            IQueryable<Book> books, 
            IQueryable<BookEvent> events,
            IQueryable<BookReview> reviews)
        {
            _books = books;
            _events = events;
            _bookReviews = reviews;
        }

        public BookEntryModel FindById(Guid id, Guid borrowerId)
            => _books
                .Where(x => x.Id == id)
                .ToBookEntriesWithLastHistory(borrowerId, includeDescription: true)
                .SingleOrDefault();

        public IEnumerable<BookEntryModel> GetAllBooks(Guid borrowerId)
            => _books
                .ToBookEntriesWithLastHistory(borrowerId)
                .ToList();


        public IEnumerable<BorrowRecordModel> GetBorrowRecords(Guid bookId) 
            => _events
                .Where(x => x.BookId == bookId)
                .ToList()
                .CreateBorrowRecords();

        public IEnumerable<BookReviewModel> GetBookReviews(Guid bookId)
           => _bookReviews
                .Where(x => x.BookId == bookId)
                .Select(x => new BookReviewModel            {
                    Title = x.Title,
                    Body = x.Body,
                    Username = x.Username,
                });
    }

    public static class StaticBookRepositoryExtensions
    {
        public static IEnumerable<BookEntryModel> ToBookEntriesWithLastHistory(this IEnumerable<Book> books, Guid borrowerId, bool includeDescription = false)
            => books.GroupJoin(StaticBookData.BookHistory, b => b.Id, h => h.BookId,
                    (book, histories) => new {
                        book,
                        lastHistory = histories
                            .OrderByDescending(h => h.Date)
                            .DefaultIfEmpty(new BookEvent() { 
                                BookId = book.Id, 
                                Date = book.DateAdded, 
                                IsIn = true, 
                                UserId = StaticBookRepository._systemAccount
                            })    
                            .FirstOrDefault()
                    }
                )
                .Select(b => BookEntryModel.Create(
                    book: b.book, 
                    lastHistory: b.lastHistory, 
                    myUserId: borrowerId,
                    includeDescription: includeDescription));


        /// <summary>
        /// Reshapes / Transposes a set of individual borrow/return records into groups of (borrow, return) records
        /// </summary>
        /// <param name="bookEvents"></param>
        /// <returns></returns>
        public static IEnumerable<BorrowRecordModel> CreateBorrowRecords(this IEnumerable<BookEvent> bookEvents)
            => bookEvents
                .OrderByDescending(h => h.Date)                
                .GroupWhile((a, b) => a.IsIn == b.IsIn) // this would address data quality issues in case of consecutive events in/out
                .Select(x => x.OrderBy(e => e.Date).First()) //take earliest even from group
                .Reverse()
                .SkipWhile(x=>x.IsIn) // skip initial returns (book cannot be returned before borrowed)
                .Select((bookEvent, idx) => new { bookEvent, idx })
                .GroupBy(x => x.idx / 2) //group by 2 (out, in)
                .Select(x => new                 {
                    returnedTime = x.Where(e => e.bookEvent.IsIn).SingleOrDefault()?.bookEvent.Date,
                    borrowedTime = x.Where(e => !e.bookEvent.IsIn).SingleOrDefault()?.bookEvent.Date,
                })
                .Select(x => new BorrowRecordModel { TimeOut = x.borrowedTime, TimeIn = x.returnedTime })                
                .ToList();
    }
}
