﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockportLibrary.InMemoryDatabase
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IEnumerable<T>> GroupWhile<T>(this IEnumerable<T> source, Func<T, T, bool> condition)
        {
            T previous = source.FirstOrDefault();
            if (previous == null)
                yield break;

            var list = new List<T>() { previous };
            foreach (T item in source.Skip(1))
            {
                if (condition(previous, item) == false)
                {
                    yield return list;
                    list = new List<T>();
                }
                list.Add(item);
                previous = item;
            }
            yield return list;
        }
    }
}
