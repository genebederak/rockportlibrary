﻿using RockportLibrary.Domain.Contracts;
using RockportLibrary.Model;
using System;
using System.Collections.Generic;

namespace RockportLibrary.InMemoryDatabase
{
    public static class StaticBookData
    {
        public static readonly ICollection<Book> StaticBooks = new List<Book> {
            new Book
            {
                Id = new Guid("a7d68ca5-2187-4c46-bf77-f65ee4974d32"),
                Title = "Clean Agile",
                SubTitle = "Back to Basics",
                Author = "Martin, Robert C.",
                ArtworkUrl = "/book-images/clean-agile.jpg",
                Description = "Agile Values and Principles for a New Generation “In the journey to all things Agile, Uncle Bob has been there, done that, and has the both the t-shirt and the scars to show for it. This delightful book is part history, part personal stories, and all wisdom. If you want to understand what Agile is and how it came to be, this is the book for you.” –Grady Booch “Bob’s frustration colors every sentence of Clean Agile, but it’s a justified frustration. What is in the world of Agile development is nothing compared to what could be. This book is Bob’s perspective on what to focus on to get to that ‘what could be.’ And he’s been there, so it’s worth listening.” –Kent Beck “It’s good to read Uncle Bob’s take on Agile. Whether just beginning, or a seasoned Agilista, you would do well to read this book. I agree with almost all of it. It’s just some of the parts make me realize my own shortcomings, dammit. It made me double-check our code coverage (85.09%).” –Jon Kern Nearly twenty years after the Agile Manifesto was first presented, the legendary Robert C. Martin (“Uncle Bob”) reintroduces Agile values and principles for a new generation–programmers and nonprogrammers alike. Martin, author of Clean Code and other highly influential software development guides, was there at Agile’s founding. Now, in Clean Agile: Back to Basics, he strips away misunderstandings and distractions that over the years have made it harder to use Agile than was originally intended. Martin describes what Agile is in no uncertain terms: a small discipline that helps small teams manage small projects . . . with huge implications because every big project is comprised of many small projects. Drawing on his fifty years’ experience with projects of every conceivable type, he shows how Agile can help you bring true professionalism to software development. Get back to the basics–what Agile is, was, and should always be Understand the origins, and proper practice, of SCRUM Master essential business-facing Agile practices, from small releases and acceptance tests to whole-team communication Explore Agile team members’ relationships with each other, and with their product Rediscover indispensable Agile technical practices: TDD, refactoring, simple design, and pair programming Understand the central roles values and craftsmanship play in your Agile team’s success If you want Agile’s true benefits, there are no shortcuts: You need to do Agile right. Clean Agile: Back to Basics will show you how, whether you’re a developer, tester, manager, project manager, or customer. Register your book for convenient access to downloads, updates, and/or corrections as they become available. See inside book for details."
            },
            new Book
            {
                Id = new Guid("d6c644f0-58c0-4044-a739-19ad90f515f7"),
                Title = "The Phoenix Project",
                SubTitle = "A Novel about IT, DevOps, and Helping Your Business Win A Novel about IT, DevOps, and Helping Your Business Win",
                Author = "Kim, Gene, Behr, Kevin, Spafford, George, IT Revolution Press",
                ArtworkUrl = "/book-images/phoenix-project.jpg",
                Description = "***Over a half-million sold! The sequel, The Unicorn Project, is coming Nov 26*** “Every person involved in a failed IT project should be forced to read this book.”—TIM O’REILLY, Founder & CEO of O’Reilly Media “The Phoenix Project is a must read for business and IT executives who are struggling with the growing complexity of IT.”—JIM WHITEHURST, President and CEO, Red Hat, Inc. Five years after this sleeper hit took on the world of IT and flipped it on it's head, the 5th Anniversary Edition of The Phoenix Project continues to guide IT in the DevOps revolution. In this newly updated and expanded edition of the bestselling The Phoenix Project, co-author Gene Kim includes a new afterword and a deeper delve into the Three Ways as described in The DevOps Handbook. Bill, an IT manager at Parts Unlimited, has been tasked with taking on a project critical to the future of the business, code named Phoenix Project. But the project is massively over budget and behind schedule. The CEO demands Bill must fix the mess in ninety days or else Bill's entire department will be outsourced. With the help of a prospective board member and his mysterious philosophy of The Three Ways, Bill starts to see that IT work has more in common with a manufacturing plant work than he ever imagined. With the clock ticking, Bill must organize work flow streamline interdepartmental communications, and effectively serve the other business functions at Parts Unlimited. In a fast-paced and entertaining style, three luminaries of the DevOps movement deliver a story that anyone who works in IT will recognize. Readers will not only learn how to improve their own IT organizations, they'll never view IT the same way again. “This book is a gripping read that captures brilliantly the dilemmas that face companies which depend on IT, and offers real-world solutions.”—JEZ HUMBLE, Co-author of Continuous Delivery, Lean Enterprise, Accelerate, and The DevOps Handbook ———— “I’m delighted at how The Phoenix Project has reshaped so many conversations in technology. My goal in writing The Unicorn Project was to explore and reveal the necessary but invisible structures required to make developers (and all engineers) productive, and reveal the devastating effects of technical debt and complexity. I hope this book can create common ground for technology and business leaders to leave the past behind, and co-create a better future together.”—Gene Kim, November 2019"

            },
            new Book
            {
                Id = new Guid("6c993d76-82d7-4b06-98db-a1b1bf93edf0"),
                Title = "Release It!",
                SubTitle = "Design and Deploy Production-Ready Software",
                Author = "Nygard, Michael T.",
                ArtworkUrl = "/book-images/release-it.jpg",
                Description = "A single dramatic software failure can cost a company millions of dollars - but can be avoided with simple changes to design and architecture. This new edition of the best-selling industry standard shows you how to create systems that run longer, with fewer failures, and recover better when bad things happen. New coverage includes DevOps, microservices, and cloud-native architecture. Stability antipatterns have grown to include systemic problems in large-scale systems. This is a must-have pragmatic guide to engineering for production systems. If you're a software developer, and you don't want to get alerts every night for the rest of your life, help is here. With a combination of case studies about huge losses - lost revenue, lost reputation, lost time, lost opportunity - and practical, down-to-earth advice that was all gained through painful experience, this book helps you avoid the pitfalls that cost companies millions of dollars in downtime and reputation. Eighty percent of project life-cycle cost is in production, yet few books address this topic. This updated edition deals with the production of today's systems - larger, more complex, and heavily virtualized - and includes information on chaos engineering, the discipline of applying randomness and deliberate stress to reveal systematic problems. Build systems that survive the real world, avoid downtime, implement zero-downtime upgrades and continuous delivery, and make cloud-native applications resilient. Examine ways to architect, design, and build software - particularly distributed systems - that stands up to the typhoon winds of a flash mob, a Slashdotting, or a link on Reddit. Take a hard look at software that failed the test and find ways to make sure your software survives. To skip the pain and get the experience...get this book."
            },
            new Book
            {
                Id = new Guid("c34ddd0c-96f4-41aa-8d62-077f91d549ff"),
                Title = "Design It!",
                SubTitle = "From Programmer to Software Architect",
                Author = "Keeling, Michael",
                ArtworkUrl = "/book-images/design-it.jpg",
                Description = "Don't engineer by coincidence-design it like you mean it! Filled with practical techniques, Design It! is the perfect introduction to software architecture for programmers who are ready to grow their design skills. Lead your team as a software architect, ask the right stakeholders the right questions, explore design options, and help your team implement a system that promotes the right -ilities. Share your design decisions, facilitate collaborative design workshops that are fast, effective, and fun-and develop more awesome software! With dozens of design methods, examples, and practical know-how, Design It! shows you how to become a software architect. Walk through the core concepts every architect must know, discover how to apply them, and learn a variety of skills that will make you a better programmer, leader, and designer. Uncover the big ideas behind software architecture and gain confidence working on projects big and small. Plan, design, implement, and evaluate software architectures and collaborate with your team, stakeholders, and other architects. Identify the right stakeholders and understand their needs, dig for architecturally significant requirements, write amazing quality attribute scenarios, and make confident decisions. Choose technologies based on their architectural impact, facilitate architecture-centric design workshops, and evaluate architectures using lightweight, effective methods. Write lean architecture descriptions people love to read. Run an architecture design studio, implement the architecture you've designed, and grow your team's architectural knowledge. Good design requires good communication. Talk about your software architecture with stakeholders using whiteboards, documents, and code, and apply architecture-focused design methods in your day-to-day practice. Hands-on exercises, real-world scenarios, and practical team-based decision-making tools will get everyone on board and give you the experience you need to become a confident software architect."

            },
            new Book
            {
                Id = new Guid("61f6c200-169e-4bef-be7f-1825566f9606"),
                Title = "Functional Programming in C#",
                SubTitle = "Classic Programming Techniques for Modern Projects",
                Author = "Sturm, Oliver",
                ArtworkUrl = "/book-images/functional-programming-in-c-sharp.jpg",
                Description = "Take advantage of the growing trend in functional programming. C# is the number-one language used by .NET developers and one of the most popular programming languages in the world. It has many built-in functional programming features, but most are complex and little understood. With the shift to functional programming increasing at a rapid pace, you need to know how to leverage your existing skills to take advantage of this trend. Functional Programming in C# leads you along a path that begins with the historic value of functional ideas. Inside, C# MVP and functional programming expert Oliver Sturm explains the details of relevant language features in C# and describes theory and practice of using functional techniques in C#, including currying, partial application, composition, memoization, and monads. Next, he provides practical and versatile examples, which combine approaches to solve problems in several different areas, including complex scenarios like concurrency and high-performance calculation frameworks as well as simpler use cases like Web Services and business logic implementation. Shows how C# developers can leverage their existing skills to take advantage of functional programming Uses very little math theory and instead focuses on providing solutions to real development problems with functional programming methods, unlike traditional functional programming titles Includes examples ranging from simple cases to more complex scenarios Let Functional Programming in C# show you how to get in front of the shift toward functional programming."

            },
            new Book
            {
                Id = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Title = "Accelerate",
                SubTitle = " The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations",
                Author = "Nicole Forsgren PhD, Jez Humble, Gene Kim",
                ArtworkUrl = "/book-images/accelerate.jpg",
                Description ="Accelerate your organization to win in the marketplace. How can we apply technology to drive business value? For years, we've been told that the performance of software delivery teams doesn't matter―that it can't provide a competitive advantage to our companies. Through four years of groundbreaking research to include data collected from the State of DevOps reports conducted with Puppet, Dr. Nicole Forsgren, Jez Humble, and Gene Kim set out to find a way to measure software delivery performance―and what drives it―using rigorous statistical methods. This book presents both the findings and the science behind that research, making the information accessible for readers to apply in their own organizations. Readers will discover how to measure the performance of their teams, and what capabilities they should invest in to drive higher performance. This book is ideal for management at every level."
            },
            new Book
            {
                Id = new Guid("96beae11-b1ce-4198-91be-be463b439c6f"),
                Title = "Domain Modeling Made Functional",
                SubTitle = "Tackle Software Complexity with Domain-Driven Design and F#",
                Author = "Scott Wlaschin",
                ArtworkUrl = "/book-images/domain-modeling-made-functional.jpg",
                Description = "You want increased customer satisfaction, faster development cycles, and less wasted work. Domain-driven design (DDD) combined with functional programming is the innovative combo that will get you there. In this pragmatic, down-to-earth guide, you'll see how applying the core principles of functional programming can result in software designs that model real-world requirements both elegantly and concisely - often more so than an object-oriented approach. Practical examples in the open-source F# functional language, and examples from familiar business domains, show you how to apply these techniques to build software that is business-focused, flexible, and high quality. Domain-driven design is a well-established approach to designing software that ensures that domain experts and developers work together effectively to create high-quality software. This book is the first to combine DDD with techniques from statically typed functional programming. This book is perfect for newcomers to DDD or functional programming - all the techniques you need will be introduced and explained. Model a complex domain accurately using the F# type system, creating compilable code that is also readable documentation---ensuring that the code and design never get out of sync. Encode business rules in the design so that you have \"compile-time unit tests,\" and eliminate many potential bugs by making illegal states unrepresentable. Assemble a series of small, testable functions into a complete use case, and compose these individual scenarios into a large-scale design. Discover why the combination of functional programming and DDD leads naturally to service-oriented and hexagonal architectures. Finally, create a functional domain model that works with traditional databases, NoSQL, and event stores, and safely expose your domain via a website or API. Solve real problems by focusing on real-world requirements for your software. What You Need: The code in this book is designed to be run interactively on Windows, Mac and Linux.You will need a recent version of F# (4.0 or greater), and the appropriate .NET runtime for your platform.Full installation instructions for all platforms at fsharp.org."
            } ,
            new Book
            {
                Id = new Guid("c5bbedec-2286-4469-89c3-8f2115fafb94"),
                Title = "Designing Data-Intensive Applications",
                SubTitle = "The Big Ideas Behind Reliable, Scalable, and Maintainable Systems",
                Author = "Martin Kleppmann",
                ArtworkUrl = "/book-images/designing-data-intensive-applications.jpg",
                Description = "Data is at the center of many challenges in system design today. Difficult issues need to be figured out, such as scalability, consistency, reliability, efficiency, and maintainability. In addition, we have an overwhelming variety of tools, including relational databases, NoSQL datastores, stream or batch processors, and message brokers. What are the right choices for your application? How do you make sense of all these buzzwords? In this practical and comprehensive guide, author Martin Kleppmann helps you navigate this diverse landscape by examining the pros and cons of various technologies for processing and storing data. Software keeps changing, but the fundamental principles remain the same. With this book, software engineers and architects will learn how to apply those ideas in practice, and how to make full use of data in modern applications. Peer under the hood of the systems you already use, and learn how to use and operate them more effectively Make informed decisions by identifying the strengths and weaknesses of different tools Navigate the trade-offs around consistency, scalability, fault tolerance, and complexity Understand the distributed systems research upon which modern databases are built Peek behind the scenes of major online services, and learn from their architectures"
            },
            new Book
            {
                Id = new Guid("81fa286f-2bf7-44fd-bd54-ad3bd6d466a6"),
                Title = "Domain-Driven Design Reference",
                SubTitle = "Definitions and Pattern Summaries ",
                Author = "Eric Evans",
                ArtworkUrl = "/book-images/domain-driven-design-reference.jpg",
                Description = "Domain-Driven Design (DDD) is an approach to software development for complex businesses and other domains. DDD tackles that complexity by focusing the team's attention on knowledge of the domain, picking apart the most tricky, intricate problems with models, and shaping the software around those models. Easier said than done! The techniques of DDD help us approach this systematically. This reference gives a quick and authoritative summary of the key concepts of DDD. It is not meant as a learning introduction to the subject. Eric Evans' original book and a handful of others explain DDD in depth from different perspectives. On the other hand, we often need to scan a topic quickly or get the gist of a particular pattern. That is the purpose of this reference. It is complementary to the more discursive books. The starting point of this text was a set of excerpts from the original book by Eric Evans, Domain-Driven-Design: Tackling Complexity in the Heart of Software, 2004 - in particular, the pattern summaries, which were placed in the Creative Commons by Evans and the publisher, Pearson Education. In this reference, those original summaries have been updated and expanded with new content. The practice and understanding of DDD has not stood still over the past decade, and Evans has taken this chance to document some important refinements. Some of the patterns and definitions have been edited or rewritten by Evans to clarify the original intent. Three patterns have been added, describing concepts whose usefulness and importance has emerged in the intervening years. Also, the sequence and grouping of the topics has been changed significantly to better emphasize the core principles. This is an up-to-date, quick reference to DDD."
            },
            new Book
            {
                Id = new Guid("d176c795-b979-4e19-ba6b-9126195da8dd"),
                Title = "Domain-Driven Design Distilled",
                SubTitle = "",
                Author = "Vaughn Vernon",
                ArtworkUrl = "/book-images/domain-driven-design-distilled.jpg",
                Description ="Domain-Driven Design (DDD) software modeling delivers powerful results in practice, not just in theory, which is why developers worldwide are rapidly moving to adopt it. Now, for the first time, there’s an accessible guide to the basics of DDD: What it is, what problems it solves, how it works, and how to quickly gain value from it. Concise, readable, and actionable, Domain-Driven Design Distilled never buries you in detail–it focuses on what you need to know to get results. Vaughn Vernon, author of the best-selling Implementing Domain-Driven Design, draws on his twenty years of experience applying DDD principles to real-world situations. He is uniquely well-qualified to demystify its complexities, illuminate its subtleties, and help you solve the problems you might encounter. Vernon guides you through each core DDD technique for building better software. You’ll learn how to segregate domain models using the powerful Bounded Contexts pattern, to develop a Ubiquitous Language within an explicitly bounded context, and to help domain experts and developers work together to create that language. Vernon shows how to use Subdomains to handle legacy systems and to integrate multiple Bounded Contexts to define both team relationships and technical mechanisms. Domain-Driven Design Distilled brings DDD to life. Whether you’re a developer, architect, analyst, consultant, or customer, Vernon helps you truly understand it so you can benefit from its remarkable power. Coverage includes What DDD can do for you and your organization–and why it’s so important The cornerstones of strategic design with DDD: Bounded Contexts and Ubiquitous Language Strategic design with Subdomains Context Mapping: helping teams work together and integrate software more strategically Tactical design with Aggregates and Domain Events Using project acceleration and management tools to establish and maintain team cadence"
            },
            new Book
            {
                Id = new Guid("84df5c7a-3ef2-45ed-8e1e-53aa4c3a61e5"),
                Title = "Clean Code",
                SubTitle = "A Handbook of Agile Software Craftsmanship",
                Author = "Robert C. Martin",
                ArtworkUrl = "/book-images/clean-code.jpg",
                Description = "Looks at the principles and clean code, includes case studies showcasing the practices of writing clean code, and contains a list of heuristics and \"smells\" accumulated from the process of writing clean code."
            },
            new Book
            {
                Id = new Guid("a99f9441-e856-4442-ac71-95944bd01614"),
                Title = "Clean Architecture",
                SubTitle = "A Craftsman's Guide to Software Structure and Design",
                Author = "Robert C. Martin",
                ArtworkUrl = "/book-images/clean-architecture.jpg",
                Description = "Building upon the success of best-sellers The Clean Coder and Clean Code, legendary software craftsman Robert C. \"Uncle Bob\" Martin shows how to bring greater professionalism and discipline to application architecture and design. As with his other books, Martin's Clean Architecture doesn't merely present multiple choices and options, and say \"use your best judgment\": it tells you what choices to make, and why those choices are critical to your success. Martin offers direct, is essential reading for every software architect, systems analyst, system designer, and software manager-- and for any programmer who aspires to these roles or is impacted by their work."
            },
            new Book
            {
                Id = new Guid("d0240cc4-655d-4c19-b186-5371a7a8bef6"),
                Title = "Implementing Domain-Driven Design",
                SubTitle = "",
                Author = "Vaughn Vernon",
                ArtworkUrl = "/book-images/implementing-domain-driven-design.jpg",
                Description = "“For software developers of all experience levels looking to improve their results, and design and implement domain-driven enterprise applications consistently with the best current state of professional practice, Implementing Domain-Driven Design will impart a treasure trove of knowledge hard won within the DDD and enterprise application architecture communities over the last couple decades.” –Randy Stafford, Architect At-Large, Oracle Coherence Product Development “This book is a must-read for anybody looking to put DDD into practice.” –Udi Dahan, Founder of NServiceBus Implementing Domain-Driven Design presents a top-down approach to understanding domain-driven design (DDD) in a way that fluently connects strategic patterns to fundamental tactical programming tools. Vaughn Vernon couples guided approaches to implementation with modern architectures, highlighting the importance and value of focusing on the business domain while balancing technical considerations. Building on Eric Evans’ seminal book, Domain-Driven Design, the author presents practical DDD techniques through examples from familiar domains. Each principle is backed up by realistic Java examples–all applicable to C# developers–and all content is tied together by a single case study: the delivery of a large-scale Scrum-based SaaS system for a multitenant environment. The author takes you far beyond “DDD-lite” approaches that embrace DDD solely as a technical toolset, and shows you how to fully leverage DDD’s “strategic design patterns” using Bounded Context, Context Maps, and the Ubiquitous Language. Using these techniques and examples, you can reduce time to market and improve quality, as you build software that is more flexible, more scalable, and more tightly aligned to business goals. Coverage includes Getting started the right way with DDD, so you can rapidly gain value from it Using DDD within diverse architectures, including Hexagonal, SOA, REST, CQRS, Event-Driven, and Fabric/Grid-Based Appropriately designing and applying Entities–and learning when to use Value Objects instead Mastering DDD’s powerful new Domain Events technique Designing Repositories for ORM, NoSQL, and other databases"
            },
            new Book
            {
                Id = new Guid("3f8e7cbf-cb28-4fb8-b6d0-6b119454fdef"),
                Title = "Working Effectively with Legacy Code",
                SubTitle = "",
                Author = "Michael Feathers",
                ArtworkUrl = "/book-images/working-effectively-with-legacy-code.jpg",
                Description ="Get more out of your legacy systems: more performance, functionality, reliability, and manageability Is your code easy to change? Can you get nearly instantaneous feedback when you do change it? Do you understand it? If the answer to any of these questions is no, you have legacy code, and it is draining time and money away from your development efforts. In this book, Michael Feathers offers start-to-finish strategies for working more effectively with large, untested legacy code bases. This book draws on material Michael created for his renowned Object Mentor seminars: techniques Michael has used in mentoring to help hundreds of developers, technical managers, and testers bring their legacy systems under control. The topics covered include Understanding the mechanics of software change: adding features, fixing bugs, improving design, optimizing performance Getting legacy code into a test harness Writing tests that protect you against introducing new problems Techniques that can be used with any language or platform—with examples in Java, C++, C, and C# Accurately identifying where code changes need to be made Coping with legacy systems that aren't object-oriented Handling applications that don't seem to have any structure This book also includes a catalog of twenty-four dependency-breaking techniques that help you work with program elements in isolation and make safer changes."
            },
            new Book
            {
                Id = new Guid("33581570-f202-4dce-8f70-142d9000f68b"),
                Title = "The Clean Coder",
                SubTitle = "A Code of Conduct for Professional Programmers",
                Author = "Robert Martin",
                ArtworkUrl = "/book-images/clean-coder.jpg",
                Description = "Programmers who endure and succeed amidst swirling uncertainty and nonstop pressure share a common attribute: They care deeply about the practice of creating software. They treat it as a craft. They are professionals. In The Clean Coder: A Code of Conduct for Professional Programmers, legendary software expert Robert C. Martin introduces the disciplines, techniques, tools, and practices of true software craftsmanship. This book is packed with practical advice–about everything from estimating and coding to refactoring and testing. It covers much more than technique: It is about attitude. Martin shows how to approach software development with honor, self-respect, and pride; work well and work clean; communicate and estimate faithfully; face difficult decisions with clarity and honesty; and understand that deep knowledge comes with a responsibility to act. Readers will learn What it means to behave as a true software craftsman How to deal with conflict, tight schedules, and unreasonable managers How to get into the flow of coding, and get past writer’s block How to handle unrelenting pressure and avoid burnout How to combine enduring attitudes with new development paradigms How to manage your time, and avoid blind alleys, marshes, bogs, and swamps How to foster environments where programmers and teams can thrive When to say “No”–and how to say it When to say “Yes”–and what yes really means Great software is something to marvel at: powerful, elegant, functional, a pleasure to work with as both a developer and as a user. Great software isn’t written by machines. It is written by professionals with an unshakable commitment to craftsmanship. The Clean Coder will help you become one of them–and earn the pride and fulfillment that they alone possess."
            },
            new Book
            {
                Id = new Guid("cdf4b491-9a61-4988-a4c8-9fc25c5a35f2"),
                Title = "Agile Principles, Patterns, and Practices in C#",
                SubTitle = "",
                Author = "Robert C. Martin",
                ArtworkUrl = "/book-images/agile-principles-patterns-and-practices.jpg",
                Description = "With the award-winning book Agile Software Development: Principles, Patterns, and Practices, Robert C. Martin helped bring Agile principles to tens of thousands of Java and C++ programmers. Now .NET programmers have a definitive guide to agile methods with this completely updated volume from Robert C. Martin and Micah Martin, Agile Principles, Patterns, and Practices in C#. This book presents a series of case studies illustrating the fundamentals of Agile development and Agile design, and moves quickly from UML models to real C# code. The introductory chapters lay out the basics of the agile movement, while the later chapters show proven techniques in action. The book includes many source code examples that are also available for download from the authors’ Web site. Readers will come away from this book understanding Agile principles, and the fourteen practices of Extreme Programming Spiking, splitting, velocity, and planning iterations and releases Test-driven development, test-first design, and acceptance testing Refactoring with unit testing Pair programming Agile design and design smells The five types of UML diagrams and how to use them effectively Object-oriented package design and design patterns How to put all of it together for a real-world project Whether you are a C# programmer or a Visual Basic or Java programmer learning C#, a software development manager, or a business analyst, Agile Principles, Patterns, and Practices in C# is the first book you should read to understand agile software and how it applies to programming in the .NET Framework."
            },
            new Book
            {
                Id = new Guid("fef4d020-a02b-46c7-a26d-576ea61b6280"),
                Title = "Code Complete",
                SubTitle = "",
                Author = "Steve McConnell",
                ArtworkUrl = "http://books.google.com/books/content?id=QnghAQAAIAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
                Description = "This practical handbook of software construction is fully updated and revised with leading-edge practices and hundreds of new code samples, illustrating the art and science of constructing software."
            },
            new Book
            {
                Id = new Guid("b4676876-409e-4b0d-8f67-0799d0279246"),
                Title = "Just Enough Software Architecture",
                SubTitle = "A Risk-Driven Approach",
                Author = "George Fairbanks",
                ArtworkUrl = "http://books.google.com/books/content?id=5UZ-AQAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                Description = "This is a practical guide for software developers, and different than other software architecture books. Here's why: It teaches risk-driven architecting. There is no need for meticulous designs when risks are small, nor any excuse for sloppy designs when risks threaten your success. This book describes a way to do just enough architecture. It avoids the one-size-fits-all process tar pit with advice on how to tune your design effort based on the risks you face. It democratizes architecture. This book seeks to make architecture relevant to all software developers. Developers need to understand how to use constraints as guiderails that ensure desired outcomes, and how seemingly small changes can affect a system's properties. It cultivates declarative knowledge. There is a difference between being able to hit a ball and knowing why you are able to hit it, what psychologists refer to as procedural knowledge versus declarative knowledge. This book will make you more aware of what you have been doing and provide names for the concepts. It emphasizes the engineering. This book focuses on the technical parts of software development and what developers do to ensure the system works not job titles or processes. It shows you how to build models and analyze architectures so that you can make principled design tradeoffs. It describes the techniques software designers use to reason about medium to large sized problems and points out where you can learn specialized techniques in more detail. It provides practical advice. Software design decisions influence the architecture and vice versa. The approach in this book embraces drill-down/pop-up behavior by describing models that have various levels of abstraction, from architecture to data structure design."
            },
            new Book
            {
                Id = new Guid("6f31e355-f480-42e0-af1e-f61a408b2296"),
                Title = "Software Systems Architecture",
                SubTitle = "Working with Stakeholders Using Viewpoints and Perspectives",
                Author = "Nick Rozanski",
                ArtworkUrl = "http://books.google.com/books/content?id=ka4QO9kXQFUC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                Description = "Software Systems Architecture, Second Edition is a highly regarded, practitioner-oriented guide to designing and implementing effective architectures for information systems. It is both a readily accessible introduction to software architecture and an invaluable handbook of well-established best practices. With this book you will learn how to Design and communicate an architecture that reflects and balances the different needs of its stakeholders Focus on architecturally significant aspects of design, including frequently overlooked areas such as performance, resilience, and location Use scenarios and patterns to drive the creation and validation of your architecture Document your architecture as a set of related views Reflecting new standards and developments in the field, this new edition extends and updates much of the content, and Adds a “system context viewpoint” that documents the system's interactions with its environment Expands the discussion of architectural principles, showing how they can be used to provide traceability and rationale for architectural decisions Explains how agile development and architecture can work together Positions requirements and architecture activities in the project context Presents a new lightweight method for architectural validation Whether you are an aspiring or practicing software architect, you will find yourself referring repeatedly to the practical advice in throughout the lifecycle of your projects. A supporting Web site containing further information can be found at www.viewpoints-and-perspectives.info."
            },
            new Book
            {
                Id = new Guid("5ef96af4-36b1-42e7-b53f-5b594d74c269"),
                    Title = "Building Evolutionary Architectures",
                    SubTitle = "Support Constant Change",
                    Author = "Neal Ford",
                    ArtworkUrl = "http://books.google.com/books/content?id=pYI2DwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                    Description = "The software development ecosystem is constantly changing, providing a constant stream of new tools, frameworks, techniques, and paradigms. Over the past few years, incremental developments in core engineering practices for software development have created the foundations for rethinking how architecture changes over time, along with ways to protect important architectural characteristics as it evolves. This practical guide ties those parts together with a new way to think about architecture and time."
            },
            new Book
            {
                Id = new Guid("ae651437-0b6e-4249-b81f-695d575bf080"),
                    Title = "Functional Programming in C#",
                    SubTitle = "",
                    Author = "Enrico Buonanno",
                    ArtworkUrl = "http://books.google.com/books/content?id=mOf7MAAACAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
                    Description = "Summary Functional Programming in C# teaches you to apply functional thinking to real-world problems using the C# language. The book, with its many practical examples, is written for proficient C# programmers with no prior FP experience. It will give you an awesome new perspective. Purchase of the print book includes a free eBook in PDF, Kindle, and ePub formats from Manning Publications. About the Technology Functional programming changes the way you think about code. For C# developers, FP techniques can greatly improve state management, concurrency, event handling, and long-term code maintenance. And C# offers the flexibility that allows you to benefit fully from the application of functional techniques. This book gives you the awesome power of a new perspective. About the Book Functional Programming in C# teaches you to apply functional thinking to real-world problems using the C# language. You'll start by learning the principles of functional programming and the language features that allow you to program functionally. As you explore the many practical examples, you'll learn the power of function composition, data flow programming, immutable data structures, and monadic composition with LINQ. What's Inside Write readable, team-friendly code Master async and data streams Radically improve error handling Event sourcing and other FP patterns About the Reader Written for proficient C# programmers with no prior FP experience. About the Author Enrico Buonanno studied computer science at Columbia University and has 15 years of experience as a developer, architect, and trainer. Table of Contents PART 1 - CORE CONCEPTS Introducing functional programming Why function purity matters Designing function signatures and types Patterns in functional programming Designing programs with function composition PART 2 - BECOMING FUNCTIONAL Functional error handling Structuring an application with functions Working effectively with multi-argument functions Thinking about data functionally Event sourcing: a functional approach to persistence PART 3 - ADVANCED TECHNIQUES Lazy computations, continuations, and the beauty of monadic composition Stateful programs and stateful computations Working with asynchronous computations Data streams and the Reactive Extensions An introduction to message-passing concurrency"
            },
            new Book
            {
                Id = new Guid("b308a180-5074-479c-804e-85f52d2d2843"),
                    Title = "Building Microservices",
                    SubTitle = "",
                    Author = "Sam Newman",
                    ArtworkUrl = "http://books.google.com/books/content?id=jjl4BgAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                    Description = "Annotation Over the past 10 years, distributed systems have become more fine-grained. From the large multi-million line long monolithic applications, we are now seeing the benefits of smaller self-contained services. Rather than heavy-weight, hard to change Service Oriented Architectures, we are now seeing systems consisting of collaborating microservices. Easier to change, deploy, and if required retire, organizations which are in the right position to take advantage of them are yielding significant benefits. This book takes an holistic view of the things you need to be cognizant of in order to pull this off. It covers just enough understanding of technology, architecture, operations and organization to show you how to move towards finer-grained systems."
            },
            new Book
            {
                Id = new Guid("feec8c30-467e-4dd7-b70c-02ed428da2e2"),
                    Title = "Adaptive Code",
                    SubTitle = "Agile coding with design patterns and SOLID principles",
                    Author = "Gary McLean Hall",
                    ArtworkUrl = "http://books.google.com/books/content?id=18SuDgAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                    Description = "Write code that can adapt to changes. By applying this book’s principles, you can create code that accommodates new requirements and unforeseen scenarios without significant rewrites. Gary McLean Hall describes Agile best practices, principles, and patterns for designing and writing code that can evolve more quickly and easily, with fewer errors, because it doesn’t impede change. Now revised, updated, and expanded, Adaptive Code, Second Edition adds indispensable practical insights on Kanban, dependency inversion, and creating reusable abstractions. Drawing on over a decade of Agile consulting and development experience, McLean Hall has updated his best-seller with deeper coverage of unit testing, refactoring, pure dependency injection, and more. Master powerful new ways to: • Write code that enables and complements Scrum, Kanban, or any other Agile framework • Develop code that can survive major changes in requirements • Plan for adaptability by using dependencies, layering, interfaces, and design patterns • Perform unit testing and refactoring in tandem, gaining more value from both • Use the “golden master” technique to make legacy code adaptive • Build SOLID code with single-responsibility, open/closed, and Liskov substitution principles • Create smaller interfaces to support more-diverse client and architectural needs • Leverage dependency injection best practices to improve code adaptability • Apply dependency inversion with the Stairway pattern, and avoid related anti-patterns About You This book is for programmers of all skill levels seeking more-practical insight into design patterns, SOLID principles, unit testing, refactoring, and related topics. Most readers will have programmed in C#, Java, C++, or similar object-oriented languages, and will be familiar with core procedural programming techniques."
                },
            new Book
            {
                Id = new Guid("1e718bd0-0163-4622-9ef2-86cb50c104a6"),
                    Title = "Domain-driven Design",
                    SubTitle = "Tackling Complexity in the Heart of Software",
                    Author = "Eric Evans",
                    ArtworkUrl = "http://books.google.com/books/content?id=xColAAPGubgC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                    Description = "Describes ways to incorporate domain modeling into software development."
            }
        };


        public static readonly ICollection<BookEvent> BookHistory = new List<BookEvent>
        {
            new BookEvent
            {
                BookId = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Date = new DateTime(2020, 1,1),
                IsIn = false,
                UserId = new Guid("22222222-2222-2222-2222-222222222222"),

            },
            new BookEvent
            {
                BookId = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Date = new DateTime(2020, 2,2),
                IsIn = true,
                UserId = new Guid("22222222-2222-2222-2222-222222222222"),

            }            ,
            new BookEvent
            {
                BookId = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Date = new DateTime(2020, 2,3),
                IsIn = false,
                UserId = new Guid("22222222-2222-2222-2222-222222222222"),

            },
            new BookEvent
            {
                BookId = new Guid("96beae11-b1ce-4198-91be-be463b439c6f"),
                Date = new DateTime(2020,2,20),
                IsIn = false,
                UserId = new Guid("11111111-1111-1111-1111-111111111111")
            },
            new BookEvent
            {
                BookId = new Guid("d6c644f0-58c0-4044-a739-19ad90f515f7"),
                Date = new DateTime(2020,2,21),
                IsIn = false,
                UserId = new Guid("11111111-1111-1111-1111-111111111111")
                
            }
        };

        public static List<BookReview> BookReviews = new List<BookReview>
        {
            new BookReview
            {
                BookId = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Title = "I liked it!",
                Body = "Great book"
            },
            new BookReview
            {
                BookId = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Title = "A good read!",
                Body = "A nice read"
            }
        };
    }
}
