﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RockportLibrary.Domain.Contracts;
using RockportLibrary.Model;
using RockportLibrary.Read;
using RockportLibrary.Read.contracts;

namespace RockportLibrary.Ui.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookViewRepository _bookViewRepository;
        private readonly IBookBorrowing _lending;
        private readonly IBookReviewing _reviewing;
        private readonly IBookAdministration _administration;


        // This is you
        private Guid CurrentUser = new Guid("11111111-1111-1111-1111-111111111111");

        public BooksController(
            IBookViewRepository bookRepository,
            IBookBorrowing lending,
            IBookReviewing reviewing,
            IBookAdministration administration,
            ILogger<BooksController> logger)
        {
            _bookViewRepository = bookRepository;
            _lending = lending;
            _reviewing = reviewing;
            _administration = administration;
        }

        [HttpGet]
        [ProducesResponseType(200)]

        public IEnumerable<BookEntryModel> Get() 
            => _bookViewRepository
                .GetAllBooks(CurrentUser);

        [HttpGet("{id:guid}")]
        public ActionResult<BookEntryModel> Get(Guid id)
        {
            var book = _bookViewRepository.FindById(id, CurrentUser);

            if (book == null)
                return NotFound($"book with id {id} was not found");

            return Ok(book);
        }

        [HttpGet("{id:guid}/history")]
        public ActionResult<IEnumerable<BookEntryModel>> History(Guid id)
        {
            var book = _bookViewRepository.GetBorrowRecords(id);

            if (book == null)
                return NotFound($"book with id {id} was not found");

            return Ok(book);
        }

        [HttpGet("{id:guid}/reviews")]
        public ActionResult<IEnumerable<BookEntryModel>> Reviews(Guid id)
        {
            var book = _bookViewRepository.GetBookReviews(id);

            if (book == null)
                return NotFound($"book with id {id} was not found");

            return Ok(book);
        }

        [HttpPost("{id:guid}/reviews")]
        public ActionResult AddReview(Guid id, BookReviewModel reviewModel)
        {
            var newReview = _reviewing.ReviewBook(new BookReview
            {
                Id = Guid.NewGuid(),
                BookId = id,
                Title = reviewModel.Title,
                Body = reviewModel.Body,
                Username = reviewModel.Username
            });

            return Ok();
        }

        [HttpPatch("{id:guid}/borrow")]
        public ActionResult Borrow(Guid id)
        {
            var isSuccess = _lending.Borrow(id, CurrentUser);

            if (!isSuccess)
                return BadRequest($"Sorry, cannot borrow this book at this time. Try another.");

            return Ok();
        }


        [HttpPatch("{id:guid}/return")]
        public ActionResult Return(Guid id)
        {
            var isSuccess = _lending.ReturnBook(id, CurrentUser);

            if (!isSuccess)
                return BadRequest($"Sorry, cannot return this book at this time. Read it again.");

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public ActionResult Delete(Guid id)
        {
            var isSuccess = _administration.DeleteBook(id);

            if (!isSuccess)
                return BadRequest($"Sorry, cannot delete this book at this time.");

            return Ok();
        }

        /// <summary>
        /// Add a book to the library
        /// </summary>
        /// <param name="book"></param>
        /// <returns>An ActionResult of BookEntry</returns>
        [HttpPost]
        
        public ActionResult<BookEntryModel> AddBook(NewBookEntry book)
        {
 
            var newBookId = _administration.Add(book);
            return CreatedAtAction(
                "Get",
                new { id = newBookId },
                _bookViewRepository.FindById(newBookId, CurrentUser));
        }
           



    }
}