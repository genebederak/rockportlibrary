using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RockportLibrary.Domain;
using RockportLibrary.Domain.Contracts;
using RockportLibrary.InMemoryDatabase;
using RockportLibrary.Read.contracts;

namespace RockportLibrary.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:8099")
                    .AllowAnyHeader()
                                .AllowAnyMethod();
                });
            });

            services.AddControllersWithViews();
            services.AddMvc(
                //setupAction =>
                //{
                //    setupAction.ReturnHttpNotAcceptable = true;
                //    var jsonOutputFormatter = setupAction.OutputFormatters.OfType<JsonOutputFormatter>().FirstOrDefault();


                //}
                )
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0); 
  
            services.AddTransient<IBookViewRepository, StaticBookRepository>(
                _ => new StaticBookRepository(
                    books: StaticBookData.StaticBooks.AsQueryable(),
                    events: StaticBookData.BookHistory.AsQueryable(),
                    reviews: StaticBookData.BookReviews.AsQueryable()));

            services.AddTransient<IBookBorrowing, BookBorrowing>(
                _ => new BookBorrowing(
                    books: StaticBookData.StaticBooks.AsQueryable(),
                    events: StaticBookData.BookHistory
                    ));

            services.AddTransient<IBookReviewing, BookReviewing>(
                _ => new BookReviewing(
                    books: StaticBookData.StaticBooks.AsQueryable(),
                    reviews: StaticBookData.BookReviews
                ));
            services.AddTransient<IBookAdministration, Administration>(
                _ => new Administration(
                    books: StaticBookData.StaticBooks
                    ));

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("OpenAPISpec", new Microsoft.OpenApi.Models.OpenApiInfo()
                {
                    Title = "Rockport Library API",
                    Version = "1"
                });

                //setupAction.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "RockportLibrary.Api.xml"));
               // setupAction.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "RockportLibrary.Read.xml"));
            });

            // 422 response ?? 
            //services.Configure<ApiBehaviorOptions>(options =>
            //{
            //    options.InvalidModelStateResponseFactory = actionContext =>
            //    {
            //        var actionExecutingContext =
            //            actionContext as Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext;

            //        if(actionContext.ModelState.ErrorCount > 0
            //            && actionExecutingContext?.ActionArguments.Count == actionContext.ActionDescriptor
            //        )
            //        {
            //            return new UnprocessableEntityObjectResult(actionContext.ModelState);
            //        }

            //    }
            //});
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/error");
            }

            app.UseCors(MyAllowSpecificOrigins);

            app.UseDefaultFiles();

            app.UseSwagger();
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint("/swagger/OpenAPISpec/swagger.json",
                    "Rockport Library API");
                setupAction.RoutePrefix = "api";
            });
            
            app.UseStaticFiles();
            app.UseRouting();
            
            app.UseEndpoints(cfg =>
            {
                
                cfg.MapControllerRoute("Fallback", "{controller}/{action}/{id?}",
                    new { controller = "App", action = "Index" });
            });

            
        }
    }
}
