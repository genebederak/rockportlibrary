﻿using Amazon.SQS;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace RockportLibrary.RandomReviewer
{
    class Program
    {
        static Random _rand = new Random();
        static void Main(string[] args)
        {



            Go().Wait();

        }

        public async static Task Go()
        {
            var configuration = new ConfigurationBuilder()
            .AddJsonFile($"appsettings.json", true, true)
            .Build();


            var serviceProvider = new ServiceCollection()
                .AddTransient<IQueueSender, AmazonQueueSender>()
                .AddAWSService<IAmazonSQS>(configuration.GetAWSOptions())
                .BuildServiceProvider();


            var sender = serviceProvider.GetService<IQueueSender>();

            while (true)
            {
                SendMessage(sender);
                await Task.Delay(1000);
            }
        }
        private static async void SendMessage(IQueueSender sender)
        {
            var r = _rand.Next(0, 10000);
            var res = sender.SendMessage(new BookReviewMessage
            {
                BookId = new Guid("15dd08b4-6934-4d64-9cd6-303eacc6e939"),
                Title = $"I Liked {r}",
                Body = $"The book was very nice {r}"
            });
            Console.WriteLine($"Sent message {r}");
            
        }
    }
}
