﻿using Amazon.SQS;
using Amazon.SQS.Model;
using System.Text.Json;
using System.Threading.Tasks;

namespace RockportLibrary.RandomReviewer
{
    public class AmazonQueueSender : IQueueSender
    {
        private readonly IAmazonSQS _amazonSQS;

        public AmazonQueueSender(Amazon.SQS.IAmazonSQS amazonSQS)
        {
            _amazonSQS = amazonSQS;
        }

        public async Task<string> SendMessage(BookReviewMessage msg)
        {
            var result = await _amazonSQS.SendMessageAsync(new SendMessageRequest
            {
                QueueUrl = "https://sqs.us-east-1.amazonaws.com/646349567016/rplibreviews-standardqueue",
                MessageBody = JsonSerializer.Serialize(msg)
            });

            return "ok";
        }
    }


}
