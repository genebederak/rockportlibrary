﻿using System.Threading.Tasks;

namespace RockportLibrary.RandomReviewer
{
    public interface IQueueSender
    {
        Task<string> SendMessage(BookReviewMessage msg);
    }


}
