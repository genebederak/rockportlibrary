﻿using System;

namespace RockportLibrary.RandomReviewer
{
    public class BookReviewMessage
    {
        public Guid BookId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}