using Microsoft.VisualStudio.TestTools.UnitTesting;
using RockportLibrary.Model;
using RockportLibrary.Read;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RockportLibrary.InMemoryDatabase;

namespace RockportLibrayCore_Tests
{
    [TestClass]
    public class BookEventsToRecordsTests
    {
        [TestMethod]
        public void GivenTwoBorrowsOver4Events_ThenDisplaysTwoRecords()
        {
            var records = new[]
            {
                new BookEvent { Date = new DateTime(2020,1,1), IsIn = false },
                new BookEvent { Date = new DateTime(2020,1,3), IsIn = true },
                new BookEvent { Date = new DateTime(2020,1,6), IsIn = false },
                new BookEvent { Date = new DateTime(2020,1,7), IsIn = true },

            }
            .CreateBorrowRecords();

            TraceBookRecords(records);
            Assert.AreEqual(2, records.Count());
        }


        [TestMethod]
        public void GivenConsecutiveOutEvents_ThenCollapsesToEarliest()
        {
            var records = new[]
            {
                new BookEvent { Date = new DateTime(2020,1,1), IsIn = false },
                new BookEvent { Date = new DateTime(2020,1,2), IsIn = false },
                new BookEvent { Date = new DateTime(2020,1,3), IsIn = true },

            }
            .CreateBorrowRecords();

            TraceBookRecords(records);
            Assert.AreEqual(1, records.Count());
            Assert.AreEqual(new DateTime(2020, 1, 1), records.First().TimeOut);
        }

        [TestMethod]
        public void GivenConsecutiveInEvents_ThenCollapsesToEarliest()
        {
            var records = new[]
            {
                new BookEvent { Date = new DateTime(2020,1,1), IsIn = false },
                new BookEvent { Date = new DateTime(2020,1,2), IsIn = true },
                new BookEvent { Date = new DateTime(2020,1,3), IsIn = true },

            }
            .CreateBorrowRecords();

            TraceBookRecords(records);
            Assert.AreEqual(1, records.Count());
            Assert.AreEqual(new DateTime(2020, 1, 2), records.First().TimeIn);
        }

        [TestMethod]
        public void GivenFirstRecordIsInRecord_then()
        {
            var records = new[]
            {
                new BookEvent { Date = new DateTime(2020,1,1), IsIn = true },
                new BookEvent { Date = new DateTime(2020,1,2), IsIn = false },
                new BookEvent { Date = new DateTime(2020,1,3), IsIn = true },

            }
            .CreateBorrowRecords();

            TraceBookRecords(records);
            Assert.AreEqual(1, records.Count());
            Assert.AreEqual(new DateTime(2020, 1, 2), records.First().TimeOut);
            Assert.AreEqual(new DateTime(2020, 1, 3), records.First().TimeIn);
        }

        [TestMethod]
        public void GivenNoReturn_ThenNullTimeIn()
        {
            var records = new[]            {
                new BookEvent { Date = new DateTime(2020,1,3), IsIn = false },
            }
            .CreateBorrowRecords();

            TraceBookRecords(records);
            Assert.AreEqual(1, records.Count());
            Assert.AreEqual(new DateTime(2020, 1, 3), records.First().TimeOut);
            Assert.AreEqual(null, records.First().TimeIn);
        }

        [TestMethod]
        public void GivenNoRecords_ThenEmpty()
        {
            var records = new BookEvent[0]
            .CreateBorrowRecords();

            TraceBookRecords(records);
            Assert.AreEqual(0, records.Count());
        }

        private void TraceBookRecords(IEnumerable<BorrowRecordModel> records)
            => records
                .ToList()
                .ForEach(x => Trace.WriteLine($"{x.TimeOut} {x.TimeIn}"));



    }
}
